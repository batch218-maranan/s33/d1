// [SECTION] Javascript Synchornous vs Asynchronous
// Javascript is by default is synchronous meaning that only one statement is executed at a time.

console.log("Hello World");
//conosle.log("Hello Again"); // Syntax Error
console.log("Goodbye");


// for (let i=0; i<=1500; i++) {
//      console.log(i);
// }


// This code will not execute while the loop code above is not yet done
// console.log("Hello Again");

// Asynchronous
// -means that we can proceed to execute other statements, while time consuming codes is running in the background

// The fetch API allows you to asynchronously request for a resource data
// A "promise" is an object taht represents the eventual completion (or failure) of an asynchronouse function and its resulting value

// Syntax: fetch('URL')

console.log(fetch('https://jsonplaceholder.typicode.com/posts'));

fetch('https://jsonplaceholder.typicode.com/posts')
.then(response => response.json())
.then(data => console.log(data));

fetch('https://jsonplaceholder.typicode.com/posts')
.then(response => console.log(response));

fetch('https://jsonplaceholder.typicode.com/posts/1')
.then(response => console.log(response));




fetch('https://jsonplaceholder.typicode.com/posts')
// The "then" method captures the "Resposne" object and returns another promise which will eventually be resolved or rejected
.then(response => console.log(response.status));


fetch('https://jsonplaceholder.typicode.com/posts')

// use the 'json' method from the "Response" object and returns another 'promise'
.then(response => response.json())
// We print converted JSON value from the fetch request
.then(json => console.log(json))

// The "async" and "await" keywords is another approach that can be used to achieve asynchronous code

async function fetchData() {
        let result = await fetch('https://jsonplaceholder.typicode.com/posts')

        console.log(result);

        let json = await result.json();
        console.log(json);

        
}

fetchData();

console.log('hhehehe');

// [SECTION] Getting a specific document

fetch('https://jsonplaceholder.typicode.com/posts/1')
.then(response => response.json())
.then(object => console.log(object))

// fetch('https://jsonplaceholder.typicode.com/posts')
// .then(response => response.json())
// .then(object => {
//      console.log(object.map(post=> post.title))
// })


fetch('https://jsonplaceholder.typicode.com/posts/87')
.then(response => response.json())
.then(object => {
        console.log(`The retrieved title "${object.title}"`);
})

// [SECTION] Creating a specific post


fetch('https://jsonplaceholder.typicode.com/posts',{
        method: 'POST',
        headers: {
                'Content-type' : 'application/json'
        },
        body: JSON.stringify({
                title: "New post",
                body: 'New outfit',
                userId: 1
        })
})

.then(response => response.json())
.then(json => console.log(json));

// [SECTION] Updating a post using PUT method

fetch('https://jsonplaceholder.typicode.com/posts/1',
{
        method: 'PUT',
        headers: {
                'Content-type' : 'application/json'
        },
        body: JSON.stringify({
                title: 'New car',
                body: 'Just got my new red tesla',
                userId: 1
        })
})
.then(response => response.json())
.then(json => console.log(json));

// [SECTION] Updating a post using PATCH method
// PUT vs PATCH
        // PATCH is used to update a single/several properties
        // PUT is used to update the whole document/object
fetch('https://jsonplaceholder.typicode.com/posts/1', {
        method: 'PATCH',
        headers: {
                'Content-type' : 'application/json'             
        },
        body: JSON.stringify({
                title : "Corrected post title"
        })
})
.then(response => response.json())
.then(json => console.log(json));

// GET, POST, PUT, PATCH, DELETE

// Deleting

fetch('https://jsonplaceholder.typicode.com/posts/1', {
        method: "DELETE",
})
.then(response => response.json())
.then(data => console.log(data))
